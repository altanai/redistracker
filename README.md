# RedisConf 2020  
## Social Network - a redis based pandemic tracker 

Tell us about yourself

1. Altanai - Backend VoIP, WebRTC and media streaming engineer at Plivo
2. Nitin Tak - Software Development Engineer at Amazon 

What’s your project about?

This project is about a Redis based tracker for the spread of a Pandemic like Covid-19. It uses efficient lookups and geospatial indexes to map the movement of uniquely identifiable persons and predict the spread based on the proximity of infected to uninfected persons.

Features :

Java project using Jedis redis client library 
Fetch an entity by its identifier and gets the metadata about that entity.
An entity can be related to other entities. Relationships between entities are out of scope but a dimension which can be added to expand use cases
For the current use case, entities would be humans.
The use case driving this would be the scope of spread of coronavirus with human interactions.

Use case

Pandemic Spread Detection and Management- A pandemic infection can happen from person to person. Depending on the type, it would have different criteria and velocity of spread. Coronavirus would be our driving use case.

Assumptions about the use case

- Pandemic spreads if people come in contact with each other physically within a distance of 6 ft.
- Having a mask might reduce the impact of spread, for the use case, prevention using mask is out of scope.
- A person infected would recover with 14 days of quarantine.
- 100% recovery is assumed with quarantine and no deaths if quarantined. If a person isn't quarantined for 14 days after being infected, they would be assumed dead.
- All people who came into proximity are invoked through API and would not account for people falling into the same geolocation path or their path intersections.

What notable metrics can you share (time saved, % faster, cost savings, etc.)?

customizability - can store wide varity of meta data in a sorted sets  provides Flexible data model 
Faster processing - maintaining a geo hash of the coordinates  is faster in a retrieval than traditional DB lookup.
High availability - Redis clusters , pooled connection and pub / sun mechanism prvides resilence to core servers 

How was Redis used in a unique way?

Redis used to store and retrieve geo data for infected and uninfected persons and  provide methods like fetch people near a person where near is defined by the radius and unit parameter.
using 

get all people belonging to a geographical region. The scope of region is defined using frame of reference for coordinate and radius with geo unit.

move a person to a destination location at a given time and update his/her geolocation 
```
jedis.geoadd(COORDINATES, metadata.getContactCoordinate().getLongitude(), metadata.getContactCoordinate().getLatitude(), uuid);
```

fetch data falling into a geo radius using geocoordinate as reference 
```
jedis.georadius(COORDINATES, center.getLongitude(), center.getLatitude(), radius, unit);
```

fetch data falling into a geo radius using person uuid as reference.
```
jedis.georadiusByMember(COORDINATES, uuid, radius, unit);
```

fetch all data for a person within given time range.
```
final Set<String> uuids = jedis.zrevrangeByScore(uuid, reference.toEpochMilli(), reference.minus(duration).toEpochMilli());
```

Can also Update timestamp of contact between for 2 people for a given to time

monitor and track infection statistics in realtime using Resid pub/sub

![screnshot for App Run](screenshots/s1.png)

Redis Schema in redis-commander

![screnshot for Console Redis](screenshots/s2.png)