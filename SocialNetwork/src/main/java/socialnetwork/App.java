package socialnetwork;

import com.google.common.collect.ImmutableSet;
import lombok.extern.log4j.Log4j2;
import model.Person;
import model.PersonLinkMetadata;
import orchestrator.Initialization;
import orchestrator.NetworkInteractionEngine;
import org.apache.commons.lang3.RandomUtils;
import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoUnit;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Hello world!
 */
@Log4j2
public final class App {

    private App() {
    }

    /**
     * Runs the simulation of pandemic spread due to social non-distancing.
     *
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        final NetworkInteractionEngine engine = new NetworkInteractionEngine();

        log.info("Creating humanity...");
        final Instant now = Instant.now();
        final Map<String, Person> population = new HashMap<>();
        IntStream.range(1, 6).forEach(j -> {
            final Instant instant = now.plus(Duration.ofDays(j)).truncatedTo(ChronoUnit.HOURS);
            IntStream.range(1, 6).forEach(i -> {
                final String uuid = String.valueOf(j) + String.valueOf(i);
                final Person person = Initialization.createPerson(uuid, false);
                population.put(uuid, person);
                engine.insertPerson(person, new GeoCoordinate(j, 0));
            });
        });
        engine.printInfectionStatistics(false, 10, true);
        log.info("Infecting uuid: 11");
        engine.infectPeople(ImmutableSet.of("11"));
        engine.printInfectionStatistics(false, 10, true);
        log.info("Location of 11 is: [{}, {}]", engine.getPersonLocation("11").get(0).getLongitude(), engine
            .getPersonLocation("11").get(0).getLatitude());
        final GeoCoordinate location21 = engine.getPersonLocation("21").get(0);
        log.info("Checking location of uuid: 21, location: [{}, {}]", location21.getLongitude(), location21.getLatitude());
        log.info("Expected to be uninfected, details of uuid 21: {}", engine.getPersonDetails("21"));
        log.info("Moving uuid: 11 to coordinates of 21");
        engine.movePersonToLocation("11", location21, now.plus(10, ChronoUnit.DAYS));
        engine.printInfectionStatistics(false, 10, true);
        log.info("Location of 11 is: [{}, {}]", engine.getPersonLocation("11").get(0).getLongitude(), engine
            .getPersonLocation("11").get(0).getLatitude());
        final Set<Person> proximatePeople = engine.getSocialProximityForUuidWithinDuration("21", Optional.of(now.plus
            (10, ChronoUnit.DAYS)), Duration.ofDays(30));
        log.info("uuid: 21 met people with uuid {} in last 7 days.", proximatePeople.stream().map
            (Person::getUuid).collect(Collectors.toList()));
        log.info("Moving uuid 51 closer to 21 now. We expect them to get sick after. Uuid 51 data: {}", engine
            .getPersonDetails("51"));
        engine.movePersonToLocation("51", location21, now.plus(10, ChronoUnit.DAYS));
        engine.printInfectionStatistics(false, 10, true);
        log.info("Printing final detailed statistics.");
        log.info("*************************.");
        engine.printInfectionStatistics(true, 100, false);
    }
}
