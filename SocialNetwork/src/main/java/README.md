# Background
This is a project for redis hackathon to use redis innovatively beyond the general caching use case.

## About SocialNetwork.

- Fetch an entity by its identifier and gets the metadata about that entity.
- An entity can be related to other entities. [Relationships between entities are out of scope but a dimension which
 can be added to expand use cases]
- For the current use case, entities would be humans.
- The use case driving this would be the scope of spread of coronavirus with human interactions.

## Use case
### Pandemic Spread Detection and Management
- A pandemic infection can happen form person to person. Depending on the type, it would have different criteria and
 velocity of spread. Coronavirus would be our driving use case.

### Assumptions about the use case
- It spreads if people come in contact with each other physically within a distance of 6 ft.
- Having a mask might reduce the impact of spread, for the use case, prevention using mask are out of scope.
- A person infected would recover with a 14 days of quarantine.
- 100% recovery is assumed with quarantine and no deaths if quarantined. If a person isn't quarantined for 14 days 
after being infected, they would be assumed dead.
- All people who came into proximity are invoked through API and would not account for people falling into same 
geolocation path or their path intersections.


References :
- Redis Labs - https://redislabs.com/redis-enterprise/data-structures/
- Jedis - https://www.baeldung.com/jedis-java-redis-client-library