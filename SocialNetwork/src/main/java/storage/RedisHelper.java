package storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import model.Person;
import model.PersonLinkMetadata;
import redis.clients.jedis.*;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Helper class to interface with redis.
 */
@Log4j2
@UtilityClass
public class RedisHelper {
    private final JedisPoolConfig poolConfig = buildPoolConfig();
    public JedisPool jedisPool = new JedisPool(poolConfig, "localhost");
    public static final String COORDINATES = "COORDINATES";
    public static final String PEOPLE = "PEOPLE";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(32);
        poolConfig.setMaxIdle(8);
        poolConfig.setMinIdle(4);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        return poolConfig;
    }

    /**
     * Fetch all population stored in redis.
     *
     * @return Set of people in redis.
     */
    public Set<Person> fetchPopulation() {
        try (final Jedis jedis = jedisPool.getResource()) {
            final Map<String, String> population = jedis.hgetAll(PEOPLE);
            final Set<Person> people = new HashSet<>();
            for (final Map.Entry e :
                    population.entrySet()) {
                people.add(MAPPER.readValue(e.getValue().toString(), Person
                        .class));
            }
            return people;
        } catch (final JsonProcessingException jpe) {
            log.error("Error in processing json for object person.", jpe);
        } catch (final Exception e) {
            log.error("Error in fetching population from redis", e);
        }
        return Collections.emptySet();
    }

    /**
     * Fetch a person with uuid.
     *
     * @param uuid identifier for the person
     * @return Person
     */
    public Person fetchPerson(final String uuid) {
        log.debug("Fetching person with UUID: {}", uuid);
        Person person = null;
        try (final Jedis jedis = jedisPool.getResource()) {
            final String serializedPerson = jedis.hget(PEOPLE, uuid);
            person = MAPPER.readValue(serializedPerson, Person.class);
            log.debug("Fetched {} from redis.", person.getName());
        } catch (final JsonProcessingException jpe) {
            log.error("Error in processing json for object person. UUID: {}", uuid, jpe);
        } catch (final Exception e) {
            log.error("Error in fetching person from redis. UUID: {}", uuid, e);
        }
        return person;
    }

    /**
     * Inserting a person into redis. The person data is serialized and written to redis using redis hash data
     * structure key.
     *
     * @param person Person to store.
     * @return number of successful update.
     */
    public void insertPerson(final Person person, final GeoCoordinate location) {
        try (final Jedis jedis = jedisPool.getResource()) {
            insertPerson(person);
            final long geoUpdated = jedis.geoadd(COORDINATES, location.getLongitude(), location
                    .getLatitude(), person.getUuid());
        } catch (final Exception e) {
            log.error("Error in inserting person to redis. Name: {}, UUID: {}", person.getName(), person
                    .getUuid(), e);
        }
    }

    /**
     * Inserting a person into redis. The person data is serialized and written to redis using redis hash data
     * structure key.
     *
     * @param person Person to store.
     * @return number of successful update.
     */
    public void insertPerson(final Person person) {
        log.debug("Inserted person with uuid: {}", person.getUuid());
        try (final Jedis jedis = jedisPool.getResource()) {
            jedis.hset(PEOPLE, person.getUuid(), MAPPER.writeValueAsString(person));
        } catch (final JsonProcessingException jpe) {
            log.error("Error in processing json for object person. Name: {}, UUID: {}", person.getName(), person
                    .getUuid(), jpe);
        } catch (final Exception e) {
            log.error("Error in inserting person to redis. Name: {}, UUID: {}", person.getName(), person
                    .getUuid(), e);
        }
    }

    /**
     * Method to update all redis entries when any group of people meet.
     *
     * @param uuids    People identifiers.
     * @param metadata Time and location data of where people met.
     */
    public void updateGroupContact(final Set<String> uuids, final PersonLinkMetadata metadata) {
        log.debug("Updating contact between {} people", uuids.size());
        try (final Jedis jedis = jedisPool.getResource()) {
            // fetch all people and see if anyone is infected. If yes, then mark all infected.
            boolean isSomeoneInfected = false;
            List<String> uuidList = uuids.stream().collect(Collectors.toList());
            for (int i = 0; i < uuidList.size(); i++) {
                final Person person = fetchPerson(uuidList.get(i));
                if (person.isInfected()) {
                    isSomeoneInfected = true;
                    log.info("Group had atleast one infected person. uuid: {}", person.getUuid());
                    break;
                }
            }
            if (isSomeoneInfected) {
                infectPeople(uuids);
            }
            for (int i = 0; i < uuidList.size(); i++) {
                // update person's location
                updateGeoDataForPerson(uuidList.get(i), metadata);
                for (int j = i + 1; j < uuidList.size(); j++) {
                    // update all people's timestamps to when they met
                    updateContactTimestampBetweenPeople(uuidList.get(i), uuidList.get(j), metadata.getTimeOfContact());
                    log.debug("{} met {} at coordinates: [{}, {}] at time: {}", uuidList.get(i), uuidList.get(j),
                            metadata
                                    .getContactCoordinate().getLongitude(), metadata
                                    .getContactCoordinate().getLatitude(), metadata
                                    .getTimeOfContact().toString());
                }
            }
        } catch (final Exception e) {
            log.error("Exception in updating contact between 2 people. Number of UUIDs: {}", uuids.size());
        }
    }

    /**
     * Method to fetch data falling into a geo radius using geocoordinate as reference.
     *
     * @param center reference geo coordinate.
     * @param radius radial distance to cover from reference.
     * @param unit   unit or measure for radius.
     * @return List of all entries belonging to that location.
     */
    public List<GeoRadiusResponse> fetchGeoDataWithinRadius(final GeoCoordinate center, final double radius, final
    GeoUnit unit) {
        List<GeoRadiusResponse> resp = Collections.emptyList();
        try (final Jedis jedis = jedisPool.getResource()) {
            resp = jedis.georadius(COORDINATES, center.getLongitude(), center.getLatitude(), radius, unit);
        } catch (final Exception e) {
            log.error("Exception in fetching UUIDs in geo query. Center coordinates: [{}, {}]. Radius: {} {}",
                    center.getLongitude(), center.getLatitude(), radius, unit.toString());
        }
        log.debug("{} people within a {} {} radius of location: [{}, {}]", resp.size(), radius, unit.toString(),
                center.getLongitude(), center.getLatitude());
        return resp;
    }

    /**
     * Method to fetch data falling into a geo radius using person uuid as reference.
     *
     * @param uuid   reference person identifier.
     * @param radius radial distance to cover from reference.
     * @param unit   unit or measure for radius.
     * @return List of all entries belonging to that location.
     */
    public List<GeoRadiusResponse> fetchGeoDataWithinRadius(final String uuid, final double radius, final GeoUnit unit) {
        List<GeoRadiusResponse> resp = Collections.emptyList();
        try (final Jedis jedis = jedisPool.getResource()) {
            resp = jedis.georadiusByMember(COORDINATES, uuid, radius, unit);
        } catch (final Exception e) {
            log.error("Exception in fetching UUIDs in geo query. Reference person UUID: {}. Radius: {} {}", uuid,
                    radius, unit.toString());
        }
        log.info("{} people within a {} {} radius of uuid: {}", resp.size(), radius, unit.toString(), uuid);
        return resp;
    }

    /**
     * Method to fetch all data for a person within given time range.
     *
     * @param uuid     identity of the person
     * @param instant  End instant for the time range. Defaults to current time.
     * @param duration Duration from the end duration into the past. For example 14 days. PT14D
     * @return
     */
    public Set<Person> fetchSocialProximityForUuidWithinDuration(final String uuid, final Optional<Instant> instant,
                                                                 final Duration duration) {
        try (final Jedis jedis = jedisPool.getResource()) {
            Instant reference = instant.isPresent() ? instant.get() : Instant.now();
            final Set<String> uuids = jedis.zrevrangeByScore(uuid, reference.toEpochMilli(), reference.minus(duration)
                .toEpochMilli());
            log.debug("{} socially proximate people for uuid: {} between {} and {}", uuids.size(), uuid, reference
                .minus(duration).toString(), reference.toString());
            return uuids.stream().map(u -> fetchPerson(u)).collect(Collectors.toSet());
        }
    }

    /**
     * Method to fetch geo location of a person.
     *
     * @param uuid identity of the person
     * @return Geolocation of that person as a list. It is expected to be of 1 size.
     */
    public List<GeoCoordinate> fetchGeoLocationOfPerson(final String uuid) {
        try (final Jedis jedis = jedisPool.getResource()) {
            final List<GeoCoordinate> coordinates = jedis.geopos(COORDINATES, uuid);
            if (coordinates.size() > 1) {
                log.warn("Fetching Geo Coordinates for a person returned more than 1 values, something is weird. " +
                        "uuid: {}", uuid);
            }
            return coordinates;
        }
    }

    /**
     * Method to infect people with uuid.
     *
     * @param uuids
     */
    public void infectPeople(final Set<String> uuids) {
        log.debug("{} people infected.", uuids.size());
        try (final Jedis jedis = jedisPool.getResource()) {
            for (final String uuid : uuids) {
                final Person person = fetchPerson(uuid);
                person.setInfected(true);
                insertPerson(person);
            }
        }
    }

    public long updateGeoDataForPerson(final String uuid, final PersonLinkMetadata metadata) {
        log.debug("Updating location for uuid: {} to location: [{},{}]", uuid, metadata.getContactCoordinate()
                .getLongitude(), metadata.getContactCoordinate().getLatitude());
        try (final Jedis jedis = jedisPool.getResource()) {
            final long resp = jedis.geoadd(COORDINATES, metadata.getContactCoordinate().getLongitude(),
                    metadata.getContactCoordinate().getLatitude(), uuid);
            return resp;
        }
    }

    private long updateContactTimestampBetweenPeople(final String uuid1, final String uuid2, final Instant instant) {
        log.debug("Updating timestamp of contact between for uuid: {} and uuid: {} to time: {}", uuid1, uuid2, instant
                .toString());
        try (final Jedis jedis = jedisPool.getResource()) {
            long resp = jedis.zadd(uuid1, instant.toEpochMilli(), uuid2);
            resp += jedis.zadd(uuid2, instant.toEpochMilli(), uuid1);
            return resp;
        }
    }
}
