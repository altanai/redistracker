package configuration;

import redis.clients.jedis.GeoUnit;

public class CoronavirusConfig {

    public static final double INFECTION_RADIUS = 6;
    public static final GeoUnit RADIUS_UNIT = GeoUnit.FT;
}
