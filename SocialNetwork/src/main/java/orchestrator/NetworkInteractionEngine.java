package orchestrator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import configuration.CoronavirusConfig;
import lombok.extern.log4j.Log4j2;
import model.Person;
import model.PersonLinkMetadata;
import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import storage.RedisHelper;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class to bind methods to the exposed API.
 */
@Log4j2
public class NetworkInteractionEngine {

    /**
     * Populate data at start.
     */
    public final Map<String, Person> populate() {
        return Initialization.populatePlanetEarth();
    }

    public void insertPerson(final Person person, final GeoCoordinate location) {
        RedisHelper.insertPerson(person, location);
    }

    /**
     * Method to invoke when people interact with each other. This could be n number of people near each other.
     *
     * @param uuids               The group or people collecting at one place.
     * @param interactionMetadata The metadata about their interaction with one another.
     */
    public void updateSocialProximity(Set<String> uuids, PersonLinkMetadata interactionMetadata) {
        log.debug("Updating interaction between a group of {} people.", uuids.size());
        RedisHelper.updateGroupContact(uuids, interactionMetadata);
    }

    public Set<Person> getSocialProximityForUuidWithinDuration(final String uuid, final Optional<Instant> instant,
                                                                 final Duration duration) {
        return RedisHelper.fetchSocialProximityForUuidWithinDuration(uuid, instant, duration);
    }

    public void infectPeople(final Set<String> people) {
        RedisHelper.infectPeople(people);
        people.stream().forEach(person -> {
            final List<GeoRadiusResponse> response = RedisHelper.fetchGeoDataWithinRadius(person, CoronavirusConfig.INFECTION_RADIUS,
                CoronavirusConfig.RADIUS_UNIT);
            RedisHelper.infectPeople(response.stream().map(e -> e.getMemberByString()).filter(e -> !e.equals(person))
                .collect(Collectors.toSet()));
        });
    }

    public Person getPersonDetails(final String uuid) {
        return RedisHelper.fetchPerson(uuid);
    }

    public List<GeoCoordinate> getPersonLocation(final String uuid) {
        return RedisHelper.fetchGeoLocationOfPerson(uuid);
    }

    /**
     * This method returns the people near a person where near is defined by the radius and unit parameter.
     *
     * @param uuid   Identity of the person in reference.
     * @param radius How far within the range of this person do we consider as "near"
     * @param unit   Unit of measure of the distance in radius.
     * @return Set of people belonging to this proximity group.
     */
    public Set<Person> getPeopleWithinRangeOfPerson(final String uuid, final double radius, final GeoUnit unit) {
        log.debug("Fetching all people within the {} {} range of uuid: {}", radius, unit.toString(), uuid);
        /**
         * Get all related people to this person.
         */
        List<GeoRadiusResponse> response = RedisHelper.fetchGeoDataWithinRadius(uuid, radius, unit);
        log.debug("Total {} people within that range.", response.size());
        final Set<Person> fetchedPeople = response.stream().map(p -> RedisHelper.fetchPerson(p.getMemberByString()))
            .collect
                (Collectors.toSet());
        log.debug("Total {} people fetched.", fetchedPeople.size());
        return fetchedPeople;
    }

    /**
     * Method to get all people belonging to a geographical region. The scope of region is defined using frame of
     * reference for coordinate and radius with geo unit.
     *
     * @param zone   reference center point coordinate
     * @param radius radial distance within which a region is considered hot zone
     * @param unit   unit of measure for distance.
     * @return
     */
    public Set<Person> getPeopleWithinZone(final GeoCoordinate zone, final double radius, final GeoUnit
        unit) {
        log.debug("Fetching all people within the {} {} range of coordinates: [{}, {}]", radius, unit.toString(), zone
            .getLongitude(), zone.getLatitude());
        final List<GeoRadiusResponse> response = RedisHelper.fetchGeoDataWithinRadius(zone, radius, unit);
        log.debug("Total {} people within that range.", response.size());
        final Set<Person> fetchedPeople = response.stream().map(p -> RedisHelper.fetchPerson(p.getMemberByString()))
            .collect
                (Collectors.toSet());
        log.debug("Total {} people fetched.", fetchedPeople.size());
        return fetchedPeople;
    }

    /**
     * Method to move a person to a destination location at a given time.
     *
     * @param uuid     Id of the person
     * @param location destination location
     * @param instant  instant of movement
     */
    public void movePersonToLocation(final String uuid, final GeoCoordinate location, final Instant instant) {
        RedisHelper.updateGeoDataForPerson(uuid, PersonLinkMetadata.builder().contactCoordinate(location)
            .timeOfContact(instant).build());
        final Set<Person> peopleInProximity = getPeopleWithinRangeOfPerson(uuid, CoronavirusConfig.INFECTION_RADIUS,
            CoronavirusConfig.RADIUS_UNIT);
        updateSocialProximity(peopleInProximity
                .stream()
                .map(Person::getUuid)
                .collect(Collectors.toSet()),
            PersonLinkMetadata
                .builder()
                .timeOfContact(instant)
                .contactCoordinate(location).build());
    }

    /**
     * Method to print infection statistics.
     *
     * @param verbosity Flag to make response more/less verbose.
     */
    public void printInfectionStatistics(final boolean verbosity, final int topN, boolean showInfectedOnly) {
        final Set<Person> population = RedisHelper.fetchPopulation();
        final Set<Person> infected = new HashSet<>();
        final Set<Person> uninfected = new HashSet<>();
        population.stream().forEach(p -> {
            if (p.isInfected()) {
                infected.add(p);
            } else {
                uninfected.add(p);
            }
        });
        log.info("Total \t Infected \t UnInfected");
        log.info("{} \t\t {} \t\t {}", population.size(), infected.size(), uninfected.size());
        if (verbosity) {
            log.info("Infected people:");
            printInfectionStatistics(infected, topN);
            if (!showInfectedOnly) {
                log.info("Uninfected people:");
                printInfectionStatistics(uninfected, topN);
            }
        }
    }

    private void printInfectionStatistics(final Set<Person> population, final int topN) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            int i = topN;
            for (final Person person :
                population) {
                if (i > 0) {
                    log.info(mapper.writeValueAsString(person));
                    i--;
                } else {
                    break;
                }
            }
        } catch (final JsonProcessingException jpe) {
            log.error("Error while writing infected person details.");
        }
    }
}
