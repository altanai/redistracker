package orchestrator;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import model.Person;
import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.Jedis;
import storage.RedisHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Log4j2
@UtilityClass
public class Initialization {

    public Map<String, Person> populatePlanetEarth() {
        /**
         * Create population.
         */
        log.info("Starting to populate earth...");
        final Map<String, Person> population = new HashMap<>();
        for (double i = 0; i <= 180; i++) {
            final Person person = createRandomPerson("_", i, new GeoCoordinate
                (i, 0));
            population.put(person.getUuid(), person);
        }
        return population;
    }

    public Person createRandomPerson(final String suffixString, final double suffixIndex, final GeoCoordinate
        location) {
        final String uuid = UUID.randomUUID().toString() + suffixString + suffixIndex;
        final Person person = Person.builder().name("name" + suffixString + suffixIndex).uuid(uuid)
            .emailAddress("name" + suffixString + suffixIndex + "@redis.com")
            .build();
//        RedisHelper.insertPerson(person, location);
        return person;
    }

    public Person createPerson(final String uuid, final boolean isInfected) {
        final Person person = Person.builder()
            .name("name_" + uuid)
            .phoneNumber("1234567890")
            .isInfected(isInfected)
            .uuid(uuid)
            .emailAddress(uuid + "@redis.com")
            .build();
        return person;
    }
}
