package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
public class Person {

    /**
     * Unique person identifier. Could be a citizen identifier in a country.
     */
    @JsonProperty
    private String uuid;
    @JsonProperty
    private String name;
    /**
     * Permanent location of a person. A family living together would have the same permanent location for all members.
     */
    // private GeoCoordinate permanentLocation;

    /**
     * Last known location of person.
     */
    // @Setter
    // private GeoCoordinate lastLocation;
    @JsonProperty
    @Builder.Default
    private int age = 30;
    @JsonProperty
    @Builder.Default
    private String mailingAddress = "The beautiful Earth";
    @JsonProperty
    @Builder.Default
    private String phoneNumber = "";
    @JsonProperty
    private String emailAddress;

    /**
     * Flag representing if a person is infected.
     */
    @JsonProperty
    @Builder.Default
    private boolean isInfected = false;
}
