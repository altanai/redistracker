package model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import redis.clients.jedis.GeoCoordinate;

import java.time.Instant;

@Builder
@Getter
@RequiredArgsConstructor
public class PersonLinkMetadata {
    private final Instant timeOfContact;
    private final GeoCoordinate contactCoordinate;
}
